function T = ChiFunc2(k,Rk,xRk,b,p)

global P0 P1 n D e;
Emat = diag(e);
Dmat = diag(ones(n,1)-e);
Pmat = P1*Emat;

% Check b is a scalar
assert(isscalar(b));
% Check distribution is indeed a vector
S = size(p);
% assert(S(2)==n);

Dbar = D+qkD(k,Rk,xRk);

T = (b^(Dbar))*(ones(1,n)*Dmat*(Pmat^qkD(k,Rk,xRk))*inv(eye(n)-b*Pmat)*(P0^D)*p);

end