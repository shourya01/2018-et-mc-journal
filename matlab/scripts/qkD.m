function T = qkD(k,Rk,xRk)

global B c D;

Q = ceil(log(xRk^2/B)/log(1/c^2)) - (k-Rk) - D;
T = max([0 Q]);

end