function T = MuFunc(b,k,Rk,xRk)

% Check b is a scalar
assert(isscalar(b));

T = (b^(k-Rk))*xRk^2;

end