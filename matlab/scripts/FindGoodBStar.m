function T = FindGoodBStar(varargin)

global a abar c M;

Mbar = M/(a^2-1);
P1 = log(a^2/abar^2);
P2 = log(a^2*c^2/abar^2);
P3 = -log(c^2);
P4 = log((-log(abar^2))/(Mbar*log(a^2)));

U = @(B) exp(P3*P4/P2)*((B)^(P1/P2));
V = @(B) log(B)/P2 + P4/P2;

B0 = exp(-P4);
Bc = Mbar*log(a^2)/log(c^2/abar^2);

F = @(B) (abar^(2*V(B)))*U(B) + Mbar*(a^(2*V(B))) - Mbar - B;

% Set tolerance for derivative
dtol = 1e-4;

% Differentiate at B0
der = (feval(@(B) F(B),B0+dtol) - feval(@(B) F(B),B0))/dtol;

if der <= 0
    T = Bc;
    return;
else
    % Set max iters
    maxiter = 1e+5;
    countiter = 1;
    while countiter < maxiter
        Bcur = B0 + countiter*dtol;
        Fval1 = feval(@(B) F(B),Bcur);
        if abs(Fval1) < dtol
            T = Bcur;
            return;
        end
        countiter = countiter + 1;
        assert(maxiter - countiter > 1,'Error in finding Bstar.');
    end
end

end
        
