function T = GkD(k,xk,xhat,Rk,xRk,p)

global a abar c B M;
zk = xk-xhat;
Mbar = M/(a^2-1);

% fprintf('Sum of p: %0.5d.\n',sum(p));

PART1 = ChiFunc(abar^2,p)*xk^2;
PART2 = 2*(ChiFunc(a*abar,p)-ChiFunc(abar^2,p))*xk*zk;
PART3 = (ChiFunc(a^2,p)-2*ChiFunc(a*abar,p)+ChiFunc(abar^2,p))*zk^2;
PART4 = Mbar*(ChiFunc(a^2,p)-ChiFunc(1,p));
PART5 = MuFunc(c^2,k,Rk,xRk)*(ChiFunc(c^2,p)-ChiFunc2(k,Rk,xRk,c^2,p));
PART6 = B*ChiFunc2(k,Rk,xRk,c^2,p);

T = PART1 + PART2 + PART3 + PART4 + PART5 - PART6;

end