function T = GoodParamValue

global a abar c B D n M;

ParamOK = 1;
Mbar = M/(a^2-1);

for i = 1:n
    del = zeros(n,1);
    del(i) = 1; % Canonical element
    V = ChiFunc(abar^2,del) - ChiFunc(c^2,del);
    W = ChiFunc(a^2,del) - ChiFunc(1,del);
    VW = V*(B/(c^(2*D))) + W*Mbar;
    if VW > 0
        ParamOK = 0;
        break;
    end
end

T = ParamOK;

end