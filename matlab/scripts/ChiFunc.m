function T = ChiFunc(b,p)

global P0 P1 n D e;
Emat = diag(e);
Dmat = diag(ones(n,1)-e);

% Check b is a scalar
assert(isscalar(b));
% Check distribution is indeed a vector
S = size(p);
% assert(S(1)==n);

% Matrices part in quadratic form
T = (b^D)*(ones(1,n)*Dmat*inv(eye(n)-b*P1*Emat)*(P0^D)*p);
end

