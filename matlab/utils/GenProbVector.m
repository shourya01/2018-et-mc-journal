function T = GenProbVector(n,varargin)

if nargin > 1
    seed = varargin{1};
    rng(seed);
else
    c = clock;
    seed = sum(c);
    rng(seed);
end

T = rand(n,1);
T = T/sum(T);

end