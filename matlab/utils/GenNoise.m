function T = GenNoise(M,steps,varargin)

if nargin > 2
    seed = varargin{1};
    rng(seed);
else
    c = clock;
    seed = sum(c);
    rng(seed);
end

assert(size(M,1)==1 && size(M,2)==1,'Variance is not a scalar.');

T = normrnd(0,M,steps,1);

end