function T = SimulateDist(p)

global n;

% Seed
c = clock;
rng(sum(c));

T = sum(rand>cumsum(p)) + 1;

assert(T>=1&&T<=n,'Bad state transition.');

end

