function T = GenMarkovMatrix(n,varargin)

if nargin > 1
    seed = varargin{1};
    rng(seed);
else
    rng('shuffle');
end

T = rand(n);

for i = 1:n
    T(:,i) = T(:,i)/sum(T(:,i));
end

end