function T = CoinFlip(p)

c = clock;
seed = sum(c);
rng('shuffle');

T = double(rand<p);

end