% Clean memory
clc;
clear all;
close all;
addpath ./scripts/ ./utils/

% A few seeds :)
seed1 = 4;
seed2 = 2;
seed3 = 22;
seed4 = 71;

% Define Parameters
global a M c abar x0 n P0 P1 e d B D Gamma0 tfinal;
a = 1.15;
M = 0.5;
c = 0.95;
abar = 0.78*c;
B = 12;
x0 = B*20;
n = 4;
P0 = GenMarkovMatrix(n,seed1);
P1 = GenMarkovMatrix(n,seed2);
% e = GenProbElemVec(n,seed3);
e = [0.2244,0.3128,0.1031,0.5400]';
d = ones(1,n) - e;
D = 3;
Gamma0 = 3;
tfinal = 500;

% Check whether channel has valid initial state
assert(Gamma0>=1 && Gamma0<=n,'Invalid channel initial state.');

% Aux. variables
Mbar = M/(a^2-1);
L = abar-a;

% Trajectory
global Traj;
p0 = zeros(n,1);
p0(Gamma0) = 1;
Traj = struct('k',0,'xk',x0,'xhat',x0,'xhatp',x0,'tk',1,'rk',0,'Gammak',Gamma0,...
    'channelprob',p0,'Rk',0,'xRk',x0,'LAC',0);

% Find if value of B is good
Bstar = FindGoodBStar;
assert(B > Bstar,'Bad Value of Bstar, condition 1.');
assert(B*log(c^2/abar^2)>Mbar*log(a^2),'Bad value of Bstar, condition 2.');

% Auxiliary Condition
Pval = GoodParamValue;
assert(Pval == 1,'Bad parameter value!');

% Find if value of D is good
% TODO

% Generate noise 
v = GenNoise(M,tfinal);

% Generate coin flip for channel change
clk = clock;
seed = sum(clk);
rng(seed);
channelCF = rand(tfinal,1);

% Start iterations
for k = 1:tfinal
    fprintf('Entered timestep %d.\n',k);
    lastval = Traj(end);
    xlast = lastval.xk;
    xhatlast = lastval.xhat;
    xhatplast = lastval.xhatp;
    tklast = lastval.tk;
    pdistlast = lastval.channelprob;
    % Evolve the system
    xcur = a*xlast + L*xhatplast + v(k); % to be stored
    % Evolve the channel
    ker = tklast*P1+(1-tklast)*P0;
    pdistcur = ker*pdistlast; % to be stored
    assert(sum(pdistcur)<=1.01,['Bad distribution on step ',num2str(k)]);
    cumpdist = cumsum(pdistcur);
    Gammacur = sum(cumpdist>channelCF(k)); % to be stored
    assert(Gammacur>=1&&Gammacur<=n,'Bad state transition.');
    % Sensing is trivial with respect to code
    rklast = lastval.rk;
    Rklast = lastval.Rk;
    xRklast = lastval.xRk;
    % Current Rk and xRk are written after rk simulation
    LAC = GkD(k,xcur,xhatlast,Rklast,xRklast,pdistcur);
%     if LAC>0
%         tkcur = 1;
%         % backoffcur to be decided after either gdk or rk is computed
%     else
%         if backofflast == 0 && rklast == 0
%             tkcur = 1; % to be stored (but not right now)
%         else
%             tkcur = 0; % to be stored (but not right now)
%         end
%     end 
    if LAC > 0
        tkcur = 1;
    else
        tkcur = 0;
    end
    % Backoff policy
    if (tklast==1 && rklast==0)
        tkcur = 1;
    end
    % BACKOFF - PERIOD WHEN GDK IS -ve
    % if backoff is 0 then it continues to be 0 unless successful reception
    %-----
    % Simulation of channel
%     tkcur = 1;
    if tkcur == 0
        rkcur = 0; % to be stored
    else
        errorprob = e(Gammacur);
        rkcur = CoinFlip(1-errorprob); % to be stored
    end
    % Current controller state
    if rkcur == 1
        xhatpcur = xcur;
        Rkcur = k;
        xRkcur = xcur;
    else
        xhatpcur = abar*xhatplast; % to be stored
        Rkcur = Rklast;
        xRkcur = xRklast;
    end
    % Decide backoff cur
    xhatcur = abar*xhatplast;
    % Save next round of data
    TrajTmp = struct('k',k,'xk',xcur,'xhat',xhatcur,'xhatp',xhatpcur,'tk',tkcur,'rk',rkcur,'Gammak',Gammacur,...
    'channelprob',pdistcur,'Rk',Rkcur,'xRk',xRkcur,'LAC',LAC);
    Traj = [Traj TrajTmp];
end

% DIAGNOSTICS CODE for FIGURES

% Set interpreters to latex
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');
set(groot, 'DefaultAxesFontSize',15);

y = [];
ye = [];
ENV = @(k) max([c^(2*k)*x0^2,B]);
for i = 0:tfinal
    y = [y Traj(i+1).xk];
    ye = [ye ENV(i+1)];
end
F1 = figure;
box on;
hold on;
plot(0:tfinal,log10(y.^2),'linewidth',2);
plot(0:tfinal,log10(ye),'--','linewidth',2);
grid on;
xlabel('Timesteps ($k$)');
ylabel('log($x_k^2$) and Envelope');
saveas(F1,'res1.png');

F2 = figure;
box on;
hold on;
tv = []; rv = [];
for i = 0:tfinal
    tv = [tv Traj(i+1).tk];
    rv = [rv Traj(i+1).rk];
end
plot(0:tfinal,tv,'bo');
plot(0:tfinal,2*rv,'ro');
grid on;
ylim([0,2.5]);
xlabel('Timesteps ($k$)');
ylabel('$t_k$ and $r_k$');
legend({'$t_k$','$r_k$'});
title(['TF is ',num2str(sum(tv)/(tfinal+1))]);
saveas(F2,'res2.png');

F3 = figure;
wl = [];
for i = 1:tfinal
    wl = [wl Traj(i+1).LAC];
end
ws = sign(wl);
box on;
hold on;
plot(1:tfinal,ws,'s','linewidth',0.5);
grid on;
ylim([-1.5,1.5]);
xlabel('Timesteps ($k$)');
ylabel('Look-ahead citeria (sign)');
saveas(F3,'res3.png');

F4 = figure;
ms = [];
for i = 0:tfinal
    ms = [ms Traj(i+1).Gammak];
end
box on;
hold on;
grid on;
plot(0:tfinal,ms,'linewidth',1);
xlabel('Timesteps ($k$)');
ylabel('Markov State');
saveas(F4,'res4.png');

close all;