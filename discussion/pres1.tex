\PassOptionsToPackage{table}{xcolor}
\documentclass[11pt,fleqn,table]{beamer}
\input{setup.tex}

\begin{document}
	
\begin{frame}{Transmission Fraction}
The \emph{running transmission fraction}, denoted by $\tau_k$, for the event-triggered policy $\Mcal{T}_{\Mcal{E}}$ is defined as-
\begin{equation*}
\tau_k := \left( \frac{1}{k} \right) \mathbb{E}_{\te} \left[ \sum_{i=1}^k t_i \right]
\end{equation*}
The \emph{infinite transmission fraction}, denoted by $\tau_\infty$, is defined as-
\begin{equation*}
\tau_\infty := \lim\limits_{k\rightarrow \infty} \tau_k
\end{equation*}
We seek to find an upper bound on the infinite transmission fraction.
\end{frame}

\begin{frame}{Transmission Fraction Strategy}

Consider the performance-evaluation function $\jdsj$ when evaluated at $S_j$ for all $j \in \mathbb{N}_0$
\begin{align*}
\jdsj =& \left[ \chipsj{\bar{a}^2} - \left(\chipsj{c^2}-\chibpsj{c^2}\right) \right] \xsqsj + \\
&\bar{M} \left[\left( \chipsj{a^2}-\chipsj{1}\right) \right]-B\chibpsj{1}
\end{align*}
Define $\Mcal{D}_1:=(0,\frac{B}{c^{2D}}]$ and $\Mcal{D}_2:=[\frac{B}{c^{2D}},x_0^2]$. By definition, $\chipsj{b}=\chibpsj{b}$
for all $b>0$ and for all $S_j$ such that $x^2_{S_j}\in \Mcal{D}_1$, and $\chipsj{b}\geq\chibpsj{b}$ 	for all $b>0$ and for all for all $S_j$ such that $x^2_{S_j}\in \Mcal{D}_2$.
\vspace{1em}
\newline
\textbf{Note that this makes the division of the squared state horizon into $\Mcal{D}_1$ and $\Mcal{D}_2$ \textit{realization
	dependent}.}
\end{frame}

\begin{frame}
It can be shown that for all $S_j$ such that $x^2_{S_j} \in \Mcal{D}_1$,
\begin{equation}
\xsqsj\chipsj{c^2} \leq	\frac{B}{c^{2D}} \chipsj{c^2}\leq B\chipsj{1} = B\chibpsj{1}
\end{equation}
Thus, replacing the $-B\chibpsj{1}$ term of $\jdsj$ with $-\frac{B}{c^{2D}}\chipsj{c^2}$, observing that 
$\chipsj{b}=\chibpsj{b}$ for all $S_j$ such that $x^2_{S_j}\in\Mcal{D}_1$, and requiring
that $\jdsj<0$ for all $S_j$ such that $x^2_{S_j}$ in $\Mcal{D}_1$, we get-
\begin{equation*}
\left( \chibpsj{\bar{a}^2}-\chibpsj{c^2}\right) \frac{B}{c^{2D}} + \left( \chibpsj{a^2}-\chibpsj{1} \right)\bar{M}<0
\end{equation*}
For all $S_j$ such that $x^2_{S_j}\in \Mcal{D}_2$, rewrite $\jdsj$ as-
\begin{align*}
\jdsj =& \left[ \chipsj{\bar{a}^2} - \chipsj{c^2} \right] \xsqsj + \bar{M} \left[\left( \chipsj{a^2}-\chipsj{1}\right) \right]\\
&+x^2_{S_j}\chibpsj{c^2}-B\chibpsj{1}
\end{align*}
For all $S_j$ such that $x^2_{S_j} \in \Mcal{D}_2$, it can be shown that $\xsqsj\chibpsj{c^2}\leq B\chibpsj{1}$ and $\jdsj$ is decreasing in $\xsqsj$, and therefore
$\jdsj<0$ for all $S_j$ such that $x^2_{S_j} \in \Mcal{D}_2$.
\end{frame}

\begin{frame}
Define $\Mcal{G}(\theta):\mathbb{N} \rightarrow [0,1]^n$ as
\begin{equation*}
\Mcal{G}(\theta):= \left(\chio{a^2}-\chio{c^2}\right) \frac{B}{c^{2\theta}} + \bar{M}\left(
\chio{a^2}-\chio{1} \right)
\end{equation*}
Further, define $\tilde{\theta}$ as
\begin{equation}
\tilde{\theta} := \argmax \{ \theta : \Mcal{G}(\theta) < \mb{0}^T \}
\end{equation}
Suppose $D+\Mcal{B} = \bar{\theta}$ where $D$ is the parameter chosen by the system designer. Then,
\begin{equation*}
 \Mcal{J}^{D+\Mcal{B}}_{S_j} = \Mcal{G}^{D}_{S_j+\Mcal{B}} < 0
\end{equation*}
Thus, after every successful reception, the sensor will back off for $\Mcal{B}$ timesteps \emph{in expectation}
under the \emph{event-triggered policy}.
\end{frame}

\begin{frame}
Now, we consider the \textbf{maximum expected time before a successful reception under continuous transmission}.
Define $\gamma_i$ as-
\begin{align*}
\gamma_i&:=\sum_{s=0}^\infty (s+1) \mb{d}^T (\mb{P}_1\Mcal{E})^{(s)} \pmb{\delta}_i \\
&= \mb{d}^T \left( (\mb{I}_n-\mb{P}_1\Mcal{E})^{-1} + (\mb{I}_n-\mb{P}_1\Mcal{E})^{-2} \right)\pmb{\delta}_i
\end{align*}
Define $\Gamma := \max_i \gamma_i$. Then $\Gamma$ is the \textbf{maximum expected time before a successful reception under continuous transmission}.
Then the upper bound on the infinite transmission fraction is given as-
\begin{equation*}
\tau_\infty \leq \frac{\Gamma}{\Gamma+\Mcal{B}}
\end{equation*}
Note that we take the \textbf{maximum} expected time because
\begin{equation*}
\frac{d}{dx} \left(\frac{x}{x+a}\right) = \frac{a}{(x+a)^2} >0 \;\;\; \forall \; x>0,a\geq0
\end{equation*}
\end{frame}

\begin{frame}{Online Choice of $D$}
We propose an algorithm for an online choice of optimal $D$ parameter at successful reception timestep $S_j$.
At timestep $k$, the lookahead function is now defined as $\Mcal{G}^{D_k}_k$. We define $D_k$ as-
\begin{equation*}
D_k=D_{R_k}
\end{equation*}
Now we only need to find the value of `online' $D_{k}$ at $k=S_j$. We `tune' the parameter $D$ at every successful reception timestep
$S_j$ according to the following rule-
\begin{equation*}
D_{S_j} = \argmax \{ \theta: \Mcal{J}^\theta_{S_j}<0 \}
\end{equation*}
\begin{lemma}
	The online assignment of $D_k$ is optimal in the sense that if the lookahead function is evaluated with some
	$D_k+i_k$ with $i_k \geq 0$ for every timestep $k$, the control goal will be violated.
\end{lemma}
\end{frame}

\begin{frame}{$\Mcal{B}$-Padding}
If $\Mcal{G}(\Mcal{B})<0$, we perform a 'padded' optimal online assignment as follows-
\begin{equation*}
D_k = D_{R_k}, \; D_{S_j}=\argmax \{ \theta:\theta\geq 0,\; \Mcal{J}^{\theta+\Mcal{B}}_{S_j}<0 \} 
\end{equation*}
The case where $D_{S_j}=0$ means continuously transmitting. However, this does not mean that the underlying
control goal is being violated, it just means that the `padded' goal is being violated.
The original control goal is always met by the ET policy by ensuring that there is at
least one nonzero $D$ such that $\Mcal{G}(D)<0$, which has been an assumption from
the start.\\
$\Mcal{B}=0$ is the most `aggressive' control, while increasing the value of $\Mcal{B}$ makes the control more `conservative'.\\
This is also the same $\Mcal{B}$ used in the upper bound for the transmission fraction.
\end{frame}

\begin{frame}{Possible Result on $\Mcal{B}$-Padding}
\begin{lemma}
	The $\Mcal{B}$-padding scheme will ensure a 'uniform' transmission fraction of
	\begin{equation*}
	\tau = \frac{\Gamma}{\Gamma+\Mcal{B}}
	\end{equation*}
	\end{lemma}
	If this lemma is true, $\Mcal{B}$ will be the 'control' which the system designer will have corresponding to
	a uniform transmission fraction across the entire trajectory.
\end{frame}

\end{document}